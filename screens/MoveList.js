import React, { Component } from 'react';
import { StyleSheet, View, Image, FlatList, ActivityIndicator } from 'react-native';
import { ListItem } from 'react-native-elements'

import MainHeader from '../components/MainHeader';

import { FullMovesAPI } from '../const/api';
import { PokemonTypeImage } from '../const/image'

export default class MoveList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      moves: [],
      isLoading: true,
      isLoadMore: false,
      nextApi: ''
    };
  }

  async componentDidMount() {
    this._fetchData(FullMovesAPI)
  }

  _fetchData = async (url) => {
    try {
      this.setState({ isLoadMore: true });

      let response = await fetch(url);
      let responseJson = await response.json();

      this.setState({
        isLoading: false,
        isLoadMore: false,
        moves: this.state.moves.concat(responseJson),
        nextApi: responseJson.next
      })
    } catch (error) {
      console.log(error);
    }
  }

  _renderItem = ({ item, index }) => {
    return <ListItem
      containerStyle={styles.listItem}
      title={item.title}
      rightElement={
        <View style={{ flexDirection: 'row' }}>
          <Image source={PokemonTypeImage[item.move_type.toLowerCase()]} />
        </View>
      }
      onPress={() => {
        this.props.navigation.navigate('MoveDetail', {
          move: this.state.moves[index]
        });
      }}
    />
  }

  _loadMoreItem = () => {
    if (this.state.nextApi) {
      this._fetchData(this.state.nextApi);
    }
  }

  _renderFooter = () => {
    if (!this.state.isLoadMore) return null;

    return (
      <ActivityIndicator animating size="large" />
    );
  };

  render() {
    const { moves, isLoading } = this.state;

    return (
      <View style={{ flex: 1 }}>
        <MainHeader title="Moves" isMain={true} navigation={this.props.navigation} />

        {!isLoading ? (
          <FlatList
            data={moves}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => String(`index-${index}-nid-${item.nid}`)}
            onEndReached={this._loadMoreItem}
            onEndReachedThreshold={0.5}
            initialNumToRender={10}
            ListFooterComponent={this._renderFooter}
          />
        ) : (
            <ActivityIndicator animating size="large" />
          )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  listItem: {
    borderBottomWidth: 1,
    borderBottomColor: '#ddd'
  }
});