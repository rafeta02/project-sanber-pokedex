import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, ActivityIndicator, Image, FlatList } from 'react-native';
import { ListItem } from 'react-native-elements'
import { FullPokemonsAPI } from '../const/api';
import { PokemonTypeImage } from '../const/image'

export default class PokemonDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pokemons: [],
            listPokemons: [],
            isLoading: true,
            isLoadMore: false,
            nextApi: '',
            search: '',
        };
    }

    componentDidMount() {
        this._fetchData(FullPokemonsAPI)
      }
    
      _fetchData = async (url) => {
        try {
          this.setState({ isLoadMore: true });
    
          let response = await fetch(url);
          let responseJson = await response.json();
          this.setState({
            isLoading: false,
            isLoadMore: false,
            pokemons: this.state.pokemons.concat(responseJson),
            listPokemons: responseJson,
            nextApi: responseJson.next,
            search: '',
          })
        } catch (error) {
          console.log(error);
        }
      }
    
      _renderItem = ({ item, index }) => {
        const pokemonTypes = item.field_pokemon_type.split(', ');
        const PokemonTypeElement = pokemonTypes.map((type, index) => {
          return (
            <Image
              key={index}
              source={PokemonTypeImage[type.toLowerCase()] || PokemonTypeImage['default']}
              placeholderStyle={{ backgroundColor: 'transparent' }}
              PlaceholderContent={<ActivityIndicator />}
            />
          )
        });
    
        return <ListItem
          containerStyle={styles.listItem}
          leftAvatar={{
            
            rounded: false,
            source: { uri: item.uri }
          }}
          title={item.title_1}
          subtitle={
            <View style={{ marginTop: 10 }}>
              <Text style={{ color: '#939393' }}>
                #{item.number.length <= 3 ? ('00' + item.number).slice(-3) : item.number}
              </Text>
            </View>
          }
          rightElement={
            <View style={{ flexDirection: 'row' }}>
              {PokemonTypeElement}
            </View>
          }
          onPress={() => {
            this.props.navigation.navigate('PokemonDetail', {
              pokemon: this.state.pokemons[index]
            });
          }}
        />
      }
    
      _renderFooter = () => {
        if (!this.state.isLoadMore) return null;
    
        return (
          <ActivityIndicator animating size="large" />
        );
      };


  render() {
    const { pokemons, isLoading } = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.body}>
          <View style={styles.content}>
        
            <Text style={styles.pokemonName}>Username</Text>
            <Text style={{alignSelf: 'center', fontSize: 21, color: '#4f4f4f', fontWeight: 'bold', marginBottom: 20}}>(Pokemon Trainer)</Text>

            <Text style={{marginBottom: 20, alignSelf: 'flex-start', fontSize: 26, color: '#4f4f4f'}}>Favorite Pokemon :</Text>
            
            {!isLoading ? (
            <FlatList
                data={pokemons}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => String(`index-${index}-nid-${item.nid}`)}
                onEndReached={this._loadMoreItem}
                onEndReachedThreshold={0.5}
                initialNumToRender={10}
                ListFooterComponent={this._renderFooter}
            />
            ) : (
                <ActivityIndicator animating size="large" />
            )}
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
  body: {
    position: 'relative',
    backgroundColor: '#559EDF',
    flex: 1,
  },
  content: {
    flex: 1,
    marginTop: 100,
    position: 'relative',
    backgroundColor: 'white',
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    padding: 20,
  },
  avatar: {
    position: 'absolute',
    width: 150,
    height: 150,
    alignSelf: 'center',
    top: -80,
  },
  pokemonName: {
    marginTop: 10,
    alignSelf: 'center',
    fontSize: 21,
    color: '#4f4f4f',
  },
  description: {
    color: '#4F4F4F',
    textAlign: 'center',
    lineHeight: 22,
    marginTop: 15,
    marginBottom: 35,
  },
  pokemonType: {
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10
  },
  pokemonStatus: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  pokemonStatusName: {
    color: '#1a87d9',
    fontWeight: 'bold',
    flex: 3,
  },
  pokemonStatusPoint: {
    paddingLeft: 10,
    paddingRight: 10,
    flex: 1,
  },
  pokemonStatusBar: {
    flex: 5,
  },
  listItem: {
    borderBottomWidth: 1,
    borderBottomColor: '#ddd'
  }
});
