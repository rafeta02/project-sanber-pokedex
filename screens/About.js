import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, ActivityIndicator, Image, TouchableOpacity, Linking } from 'react-native';
import { Icon } from 'react-native-elements';
import ProgressBar from 'react-native-progress/Bar';

export default class PokemonDetail extends Component {
  render() {

    return (
      <ScrollView>
        <View style={styles.body}>

          <View style={styles.content}>
            <Image
              style={styles.avatar}
              placeholderStyle={{backgroundColor: 'transparent'}}
              PlaceholderContent={<ActivityIndicator />}
              source={require('../images/daco2.png')} 
              resizeMode="contain"
            />

            <Text style={styles.pokemonName}>Ramadhan Febri Utama</Text>
            <Text style={{alignSelf: 'center', fontSize: 21, color: '#4f4f4f', fontWeight: 'bold', marginBottom: 10}}>(React Native Developer)</Text>


            <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10 }}>
                <TouchableOpacity onPress={ ()=>{ Linking.openURL('https://web.facebook.com/rafeta02/')}}>
                    <View style={styles.pokemonType}>
                        <Image source={require('../assets/images/facebook.png')} resizeMode="contain" />
                        <Text>Facebook</Text> 
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={ ()=>{ Linking.openURL('https://twitter.com/Ramadhanfebri')}}>
                    <View style={styles.pokemonType}>
                        <Image source={require('../assets/images/twitter.png')} resizeMode="contain" />
                        <Text>Twitter</Text> 
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={ ()=>{ Linking.openURL('https://www.instagram.com/ramadhanfebriutama/')}}>
                    <View style={styles.pokemonType}>
                        <Image source={require('../assets/images/instagram.png')} resizeMode="contain" />
                        <Text>Instagram</Text> 
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={ ()=>{ Linking.openURL('https://gitlab.com/rafeta02/')}}>
                    <View style={styles.pokemonType}>
                        <Image source={require('../assets/images/gitlab.png')} resizeMode="contain" />
                        <Text>Gitlab</Text> 
                    </View>
                </TouchableOpacity>
            </View>

            <View>
              <Text style={styles.description}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer augue sem, fermentum eget volutpat sit amet, dapibus et risus. Aliquam convallis magna vitae eleifend egestas. Fusce orci augue, pellentesque et facilisis sit amet, ultricies vitae sapien. Donec vehicula eleifend nulla, et scelerisque felis blandit quis. Proin enim felis, porta vitae odio molestie, accumsan rhoncus mi.
              </Text>
            </View>

            <Text style={{marginBottom: 20, alignSelf: 'center', fontSize: 21, color: '#4f4f4f'}}>Skills</Text>

            <View>
              <View style={styles.pokemonStatus}>
                <Text style={styles.pokemonStatusName}>Laravel</Text>
                <Text style={styles.pokemonStatusPoint}>75%</Text>
                <View style={styles.pokemonStatusBar}>
                  <ProgressBar progress={75 / 100} height={8} color='#559EDF' unfilledColor='#F0F0F0' borderWidth={0} animated={true} width={null} />
                </View>
              </View>
              <View style={styles.pokemonStatus}>
                <Text style={styles.pokemonStatusName}>Angular</Text>
                <Text style={styles.pokemonStatusPoint}>85%</Text>
                <View style={styles.pokemonStatusBar}>
                  <ProgressBar progress={85 / 100} height={8} color='#559EDF' unfilledColor='#F0F0F0' borderWidth={0} animated={true} width={null} />
                </View>
              </View>
              <View style={styles.pokemonStatus}>
                <Text style={styles.pokemonStatusName}>Ionic</Text>
                <Text style={styles.pokemonStatusPoint}>75%</Text>
                <View style={styles.pokemonStatusBar}>
                  <ProgressBar progress={75 / 100} height={8} color='#559EDF' unfilledColor='#F0F0F0' borderWidth={0} animated={true} width={null} />
                </View>
              </View>
              <View style={styles.pokemonStatus}>
                <Text style={styles.pokemonStatusName}>React Native</Text>
                <Text style={styles.pokemonStatusPoint}>60%</Text>
                <View style={styles.pokemonStatusBar}>
                  <ProgressBar progress={60 / 100} height={8} color='#559EDF' unfilledColor='#F0F0F0' borderWidth={0} animated={true} width={null} />
                </View>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  backButton: {
    alignItems: 'flex-start',
    paddingTop: 40,
    marginBottom: -40
  },
  body: {
    position: 'relative',
    backgroundColor: '#559EDF',
    flex: 1,
  },
  content: {
    flex: 1,
    marginTop: 200,
    position: 'relative',
    backgroundColor: 'white',
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    padding: 20,
  },
  avatar: {
    position: 'absolute',
    width: 200,
    height: 200,
    alignSelf: 'center',
    top: -130,
  },
  pokemonName: {
    marginTop: 90,
    alignSelf: 'center',
    fontSize: 21,
    // fontFamily: 'Avenir',
    color: '#4f4f4f',
  },
  description: {
    color: '#4F4F4F',
    textAlign: 'center',
    lineHeight: 22,
    marginTop: 15,
    marginBottom: 35,
  },
  pokemonType: {
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10
  },
  pokemonStatus: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  pokemonStatusName: {
    color: '#1a87d9',
    fontWeight: 'bold',
    flex: 3,
  },
  pokemonStatusPoint: {
    paddingLeft: 10,
    paddingRight: 10,
    flex: 1,
  },
  pokemonStatusBar: {
    flex: 5,
  },
});
