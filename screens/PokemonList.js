import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, FlatList, ActivityIndicator } from 'react-native';
import { ListItem, SearchBar } from 'react-native-elements'

import MainHeader from '../components/MainHeader';

import { FullPokemonsAPI } from '../const/api';
import { PokemonTypeImage } from '../const/image'

export default class PokemonList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pokemons: [],
      listPokemons: [],
      isLoading: true,
      isLoadMore: false,
      nextApi: '',
      search: '',
    };
  }

  componentDidMount() {
    this._fetchData(FullPokemonsAPI)
  }

  _fetchData = async (url) => {
    try {
      this.setState({ isLoadMore: true });

      let response = await fetch(url);
      let responseJson = await response.json();
      this.setState({
        isLoading: false,
        isLoadMore: false,
        pokemons: this.state.pokemons.concat(responseJson),
        listPokemons: responseJson,
        nextApi: responseJson.next,
        search: '',
      })
    } catch (error) {
      console.log(error);
    }
  }

  _renderItem = ({ item, index }) => {
    const pokemonTypes = item.field_pokemon_type.split(', ');
    const PokemonTypeElement = pokemonTypes.map((type, index) => {
      return (
        <Image
          key={index}
          source={PokemonTypeImage[type.toLowerCase()] || PokemonTypeImage['default']}
          placeholderStyle={{ backgroundColor: 'transparent' }}
          PlaceholderContent={<ActivityIndicator />}
        />
      )
    });

    return <ListItem
      containerStyle={styles.listItem}
      leftAvatar={{
        
        rounded: false,
        source: { uri: item.uri }
      }}
      title={item.title_1}
      subtitle={
        <View style={{ marginTop: 10 }}>
          <Text style={{ color: '#939393' }}>
            #{item.number.length <= 3 ? ('00' + item.number).slice(-3) : item.number}
          </Text>
        </View>
      }
      rightElement={
        <View style={{ flexDirection: 'row' }}>
          {PokemonTypeElement}
        </View>
      }
      onPress={() => {
        this.props.navigation.navigate('PokemonDetail', {
          pokemon: this.state.pokemons[index]
        });
      }}
    />
  }

  _updateSearch = search => {
    if (search == '') {
      this.setState({ search: search, pokemons: this.state.listPokemons })
    } else {
      let found = this.state.listPokemons.filter(x => {
        return x.title_1.toLowerCase().includes(search.toLowerCase())
      })
      this.setState({ search: search, pokemons: found })
    }
  };

  _loadMoreItem = () => {
    if (this.state.nextApi) {
      this._fetchData(this.state.nextApi);
    }
  }

  _renderFooter = () => {
    if (!this.state.isLoadMore) return null;

    return (
      <ActivityIndicator animating size="large" />
    );
  };

  render() {
    const { pokemons, isLoading, search } = this.state;

    return (
      <View style={{ flex: 1 }}>
        <MainHeader title="Pokemons" isMain={true} navigation={this.props.navigation} />

        <SearchBar
          placeholder='Find Pokemon by name ...'
          inputContainerStyle={{ backgroundColor: '#e9e9e9' }}
          containerStyle={{ backgroundColor: 'transparent' }}
          lightTheme={true} round={true}
          onChangeText={this._updateSearch}
          value={search}
        />

        {!isLoading ? (
          <FlatList
            data={pokemons}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => String(`index-${index}-nid-${item.nid}`)}
            onEndReached={this._loadMoreItem}
            onEndReachedThreshold={0.5}
            initialNumToRender={10}
            ListFooterComponent={this._renderFooter}
          />
        ) : (
            <ActivityIndicator animating size="large" />
          )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  listItem: {
    borderBottomWidth: 1,
    borderBottomColor: '#ddd'
  }
});