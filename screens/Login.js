import React, { Component } from 'react';
import {
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  TextInput,
  View,
  ImageBackground
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export default class Login extends Component {
    constructor(props) {
        super(props)

        this.state = {
            userName: '',
            password: '',
            message: 'Error',
            isError: false,
        }
    }

    loginHandler() {
        if (this.state.username === '' || this.state.password === '' ) {
            this.setState({ message: 'Username atau password Kosong !'});
            this.setState({ isError: true });
        } else if (this.state.password !== '12345678') {
          this.setState({ message: 'Password Salah (seharusnya "12345678") !'});
          this.setState({ isError: true });
        } else {
          this.setState({ isError: false });
          this.props.navigation.navigate('Tabs');
        }
      }

    render() {
        return(
            <ImageBackground source={require("../images/background.jpg")} resizeMode = "cover" style={{flex:1, alignItems:'center', justifyContent:'center'}} >
                <Image source={require('../assets/images/logo.png')} resizeMode="contain" style={{width:220, height: 180}} />
                <View style={{width:310, height:250, margin:10, }}>
                    <View style={{flex:1, borderColor:'#d3d3d3'}}>
                        <TextInput 
                        placeholder = "USERNAME"
                        placeholderTextColor = "#fff"
                        onChangeText={userName => this.setState({ userName })}
                        style = {styles.textInput}/>
                    </View>
                    <View style={{flex:1, borderColor:'#d3d3d3'}}>
                        <TextInput 
                        placeholder = "PASSWORD"
                        placeholderTextColor = "#fff"
                        onChangeText={password => this.setState({ password })}
                        secureTextEntry={true}
                        style = {styles.textInput}/>
                    </View>
                    <TouchableOpacity onPress={() => this.loginHandler()} style={{backgroundColor:'rgba(226,39,44, 0.5)', flex:1, justifyContent:'center', alignItems:'center'}}>
                        <Text style={{color:'#fff', fontSize:12, fontWeight:'700'}}>LOG IN</Text>
                    </TouchableOpacity>
                    <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>{this.state.message}</Text>
                    <View style={styles.row}>
                        <View style={styles.line} />
                        <View style={{flex:1, margin:5}}>
                            <Text style={{fontSize:10, fontWeight:'600', color:'#444'}}>OR CONNECT WITH</Text>
                        </View>
                        <View style = {styles.line} />
                    </View>
                    <View style={styles.row}>
                        <TouchableOpacity style = {styles.social} onPress={() => this.props.navigation.navigate('Tabs')}>
                            <Icon name = 'logo-facebook' color="#fff" style = {{margin:5}} />
                            <Text style={{color:'#fff', fontSize:11, fontWeight:'600'}}>facebook</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.social} onPress={() => this.props.navigation.navigate('Tabs')}>
                            <Icon name = 'logo-twitter' color="#fff" style = {{margin:5}} />
                            <Text style={{color:'#fff', fontSize:11, fontWeight:'600'}}>twitter</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.social} onPress={() => this.props.navigation.navigate('Tabs')}>
                            <Icon name = 'logo-googleplus' color="#fff" style = {{margin:5}} />
                            <Text style={{color:'#fff', fontSize:11, fontWeight:'600'}}>google</Text>
                        </TouchableOpacity>
                    </View>
                </View>                
            </ImageBackground>
        );
    }
};

const styles = StyleSheet.create({
    container: { flex: 1, justifyContent: 'center', alignItems: 'center' },
    textInput: {flex:1, backgroundColor:'#rgba(0,0,0,0.3)', padding:10, color:'#fff', fontSize:12},
    line:{flex:1, height:2, backgroundColor:'rgba(0,0,0,0.2)' },
    row:{flex:1, flexDirection:'row', alignItems:'center'},
    social:{flex:1, borderRadius:2, backgroundColor:'#rgba(0,0,0,0.6)', flexDirection:'row', margin:8, alignItems:'center', justifyContent:'center', padding:5},
    errorText: {
        color: 'red',
        textAlign: 'center',
        marginTop: 10,
    },
    hiddenErrorText: {
        color: 'transparent',
        textAlign: 'center',
        marginTop: 10,
    }
});