import { LOGIN } from '../actions/types';

const initialState = {
  username: 'Username',
  isLogin: false
};

export default (state = initialState, action) => {
  switch(action.type) {
    case LOGIN:
      return {helloText: action.payload, isLogin: true}
    default:
      return state;
  }
}