import { combineReducers } from 'redux';
import UsernameReducer from './usernameReducer';

export default combineReducers({
  username: UsernameReducer
})