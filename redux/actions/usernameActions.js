import { LOGIN } from './types';

export const loginAction = () => {
  return {
    type: LOGIN
  };
}