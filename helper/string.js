export function capitalizeWord(string) {
  return string.charAt(0).toUpperCase() + string.slice(1)
}

export function capitalizeSentenceFromSpinalCase(string) {
  return string.split('-').map(word => capitalizeWord(word)).join(' ')
}