import * as React from 'react';
import { Text, View, Button, Platform, StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'; 
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import About from './screens/About';
import Favorite from './screens/Favorite';
import Login from './screens/Login';
import PokemonList from './screens/PokemonList'
import MoveList from './screens/MoveList'
import PokemonDetail from './screens/PokemonDetail'
import MoveDetail from './screens/MoveDetail'

const HomeStack = createStackNavigator();

function HomeStackScreen() {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen name="Home" component={PokemonList} options={{headerShown: false}} />
      <HomeStack.Screen name="PokemonDetail" component={PokemonDetail} options={{headerShown: false}} />
    </HomeStack.Navigator>
  );
}

const MovesStack = createStackNavigator();

function MovesStackScreen() {
  return (
    <MovesStack.Navigator>
      <MovesStack.Screen name="Moves" component={MoveList} options={{headerShown: false}} />
      <MovesStack.Screen name="MoveDetail" component={MoveDetail} options={{headerShown: false}} />
    </MovesStack.Navigator>
  );
}

const TabsStack = createBottomTabNavigator();

function TabStackScreen() {
  return (
    <TabsStack.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            if (route.name === 'Home') {
              return (
                <MaterialCommunityIcons
                  name={
                    focused
                      ? 'pokemon-go'
                      : 'pokemon-go'
                  }
                  size={size}
                  color={color}
                />
              );
            } else if (route.name === 'Moves') {
              return (
                <Ionicons
                  name={focused ? 'ios-disc' : 'ios-disc'}
                  size={size}
                  color={color}
                />
              );
            } else if (route.name === 'Favorite') {
              return (
                <MaterialIcons name={focused ? 'favorite' : 'favorite-border'} size={size} color={color}></MaterialIcons>
              );
            } else if (route.name === 'About') {
              return (
                <Ionicons
                  name={focused ? 'ios-person' : 'ios-person'}
                  size={size}
                  color={color}
                />
              );
            }
          },
        })}
        tabBarOptions={{
          activeTintColor: '#000000',
          inactiveTintColor: '#00000077',
        }}
      >
      <TabsStack.Screen name="Home" component={HomeStackScreen} />
        <TabsStack.Screen name="Moves" component={MovesStackScreen} />
        <TabsStack.Screen name="Favorite" component={Favorite} />
        <TabsStack.Screen name="About" component={About} />
    </TabsStack.Navigator>
  );
}


const PrimaryStack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <PrimaryStack.Navigator initialRouteName='Login'>
        <PrimaryStack.Screen name="Login" component={Login} options={{headerShown: false}} />
        <PrimaryStack.Screen name="Tabs" component={TabStackScreen} options={{headerShown: false}}/>
      </PrimaryStack.Navigator>
    </NavigationContainer>
  );
}