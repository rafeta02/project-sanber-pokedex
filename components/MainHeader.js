import React, { Component } from 'react';
import { Header, Icon } from 'react-native-elements'

export default class MainHeader extends Component {
  render() {
    if (this.props.isMain) {
      return (
        <Header
          centerComponent={{ text: this.props.title, style: { color: '#fff' } }}
          backgroundColor='#559EDF'
        />
      )
    } else {
      return (
        <Header
          leftComponent={
            <Icon
              name='keyboard-arrow-left'
              color='#fff'
              onPress={() => {
                this.props.navigation.goBack()
              }}
            />
          }
          centerComponent={{ text: this.props.title, style: { color: '#fff' } }}
        />
      )
    }
  }
}